from machine import Pin,ADC,PWM
from utime import ticks_add,ticks_diff, ticks_ms, sleep

#ENTRADAS
ldr = ADC(27) # Creamos el objeto ADC en el GPIO 27
botonApertura = Pin(0, Pin.IN, Pin.PULL_UP)
botonCierre = Pin(1, Pin.IN, Pin.PULL_UP)
fdcIzq = Pin(14, Pin.IN, Pin.PULL_UP)
fdcDer = Pin(15, Pin.IN, Pin.PULL_UP)

#SALIDAS
onboard_led = Pin(25, Pin.OUT) 
servo = PWM(Pin(16))

#VARIABLES
izquierda = -65000
stop = 0
derecha = 65000
limite = 60000 #65000 = Noche; 0 = dia
deadline = ticks_ms()
delta = 10*60*1000 # Tiempo entre medidas del sensor: MINUTOS*60*1000ms
#delta = 5*1000 # Tiempo entre medidas del sensor: SEGUNDOS*1000ms

#PROGRAMA
servo.duty_u16(stop)        #Parar servo
medida = ldr.read_u16()     #Leer resistencia

#Parpadeo para indicar el comienzo del programa
onboard_led.on()
sleep(2)
onboard_led.off()
sleep(1)

while True:
  #Medidas cada cierto tiempo "delta"
  if ticks_diff(deadline, ticks_ms()) < 0:
    medida = ldr.read_u16()
    print('Nueva medida: ', medida)
    deadline = ticks_add(ticks_ms(), delta)

  if medida>limite-1000 and medida<limite :
    onboard_led.on()
    sleep(1)
    onboard_led.off()
    sleep(1)


#Cerrar puerta si es de noche o pulsamos el boton cerrar
  if (medida>limite or botonCierre.value()==0) and fdcIzq.value()==1 :
      print('Noche :', medida)
      onboard_led.on()
      servo.duty_u16(izquierda)
      print('Cerrando')
      while fdcIzq.value() :
        pass
      servo.duty_u16(stop)
      print('Puerta Cerrada')

#Abrir puerta si es de dia o pulsamos el boton abrir
  if (medida<=limite or botonApertura.value()==0) and fdcDer.value()==1:
    print('Dia :', medida)
    onboard_led.off()
    servo.duty_u16(derecha)
    print('Abriendo')
    while fdcDer.value() :
      pass
    servo.duty_u16(stop)
    print('Puerta Abierta')