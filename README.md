# Gallinero Pico
![PCB_render](images\PCB_3D_render.jpg)

## Presentacion del proyecto


## Documentacion utilizada:

Pi Pico Pinout y Documentacion:
- https://datasheets.raspberrypi.com/pico/Pico-R3-A4-Pinout.pdf
- https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf
- https://datasheets.raspberrypi.com/pico/pico-datasheet.pdf

Micropython Pi Pico Francés:
- https://www.micropython.fr/port_pi_pico/

Ejemplo Servo angulo:
- https://www.upesy.fr/blogs/tutorials/pi-pico-servo-motor-sg90-on-micropython
- https://docs.micropython.org/en/latest/rp2/quickref.html#pwm-pulse-width-modulation

Kicad Simbols raspberrypi Pico library
- https://github.com/ncarandini/KiCad-RP-Pico

    